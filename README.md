# SaQC Course

The available [jupyter notebook](https://git.ufz.de/rdm/saqccourse/-/blob/main/saqccourse.ipynb) provides interactive learning material for the [System for Automated Quality Control - SaQC]( https://rdm-software.pages.ufz.de/saqc/). The material was created and used for a one-day on-site training course, but should also be usable for self study purposes.

We also provide a number of [exercises](https://git.ufz.de/rdm/saqccourse/-/tree/main/exercises) to accompany, practice and deepen the topics  

The easiest way to use the course material within a working environment is to run the notebooks using [binder](https://mybinder.org/). To do so, simply click the following batch (and be patient): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.ufz.de%2Frdm%2Fsaqccourse/HEAD?labpath=saqccourse.ipynb)
